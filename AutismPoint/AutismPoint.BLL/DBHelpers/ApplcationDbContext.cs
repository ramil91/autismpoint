﻿using AutismPoint.BLL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutismPoint.BLL.DBHelpers
{
    public class ApplcationDbContext : DbContext
    {
        public DbSet<ApplicationUser> ApplicationUser { get; set; }

        public DbSet<AddClient> tblAddClient { get; set; }
        public DbSet<AddServices> tblAddServices { get; set; }
        public DbSet<BookAppointments> tblBookAppointments { get; set; }

        public ApplcationDbContext(DbContextOptions<ApplcationDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            //new CustomerMap(modelBuilder.Entity<Customer>());
        }

    }
}


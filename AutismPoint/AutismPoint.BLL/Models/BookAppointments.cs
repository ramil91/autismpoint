﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutismPoint.BLL.Models
{
    public class BookAppointments : BaseModel
    {
        [Key]
        public int Id { get; set; }
        public int ConsultantID { get; set; }
        public int ClientID { get; set; }
        public int ServiceID { get; set; }
        public DateTime MyProperty { get; set; }
    }
}

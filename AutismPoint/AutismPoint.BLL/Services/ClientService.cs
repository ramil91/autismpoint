﻿using AutismPoint.BLL.DBHelpers;
using AutismPoint.BLL.Models;
using AutismPoint.BLL.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutismPoint.BLL.Services
{
    public class ClientService : IClientService
    {
        private readonly ApplcationDbContext _context;
        public ClientService(ApplcationDbContext context)
        {
            _context = context;
        }
        public async Task SaveClientAsync(AddClient addClient)
        {
            try
            {
                await _context.AddAsync(addClient);
                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

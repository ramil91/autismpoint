﻿using AutismPoint.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutismPoint.BLL.ViewModel
{
    public class AddServicesViewModel : BaseModel
    {
        public int Id { get; set; }
        public string ServiceName { get; set; }
    }
}

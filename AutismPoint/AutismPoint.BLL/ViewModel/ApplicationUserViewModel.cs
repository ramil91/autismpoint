﻿using AutismPoint.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutismPoint.BLL.ViewModel
{
    public class ApplicationUserViewModel:BaseModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LasttName { get; set; }

        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }

        public string Password { get; set; }
        public string Designation { get; set; }
    }
}

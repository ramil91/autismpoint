﻿using AutismPoint.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutismPoint.BLL.ViewModel
{
    public class BookAppointmentsViewModel: BaseModel
    {
        public int Id { get; set; }
        public int ConsultantID { get; set; }
        public int ClientID { get; set; }
        public int ServiceID { get; set; }
        public DateTime MyProperty { get; set; }
    }
}

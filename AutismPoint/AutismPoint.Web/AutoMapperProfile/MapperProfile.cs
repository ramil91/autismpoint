﻿using AutismPoint.BLL.Models;
using AutismPoint.BLL.ViewModel;
using AutoMapper;

namespace AutismPoint.AutoMapperProfile
{


    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<ApplicationUser, ApplicationUserViewModel>();
            CreateMap<AddClient, AddClientViewModel>();
            CreateMap<AddServices, AddServicesViewModel>();
            CreateMap<BookAppointments, BookAppointmentsViewModel>();
        }
    }
}

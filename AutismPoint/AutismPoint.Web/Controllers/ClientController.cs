﻿using AutismPoint.BLL.Models;
using AutismPoint.BLL.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace AutismPoint.Web.Controllers
{
    public class ClientController : Controller
    {
        private readonly IClientService _clientService;
        public ClientController(IClientService clientService)
        {
            _clientService = clientService;

        }
        public IActionResult AddClient()
        {
            return View();
        }
        [HttpPost]
        public async Task SaveClientAsync([FromBody] AddClient client)
        {
            await _clientService.SaveClientAsync(client);
            //return 1;
        }

    }
}

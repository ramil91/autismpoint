﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutismPoint.Web.Models
{
    public class AddClient 
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Age { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNo { get; set; }
        public string vAddress { get; set; }
        public string Diagnosis { get; set; }
    }
}

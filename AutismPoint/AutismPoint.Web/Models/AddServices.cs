﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutismPoint.BLL.Models
{
    public class AddServices : BaseModel
    {
        [Key]
        public int Id { get; set; }
        public string ServiceName { get; set; }
    }
}
